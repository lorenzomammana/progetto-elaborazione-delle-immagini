function [fenstrings, detectedchessboards, detected] = ...
    extractfenfromimage(image, pretty)

[c, detectedchessboards] = chessboarddetector(image, pretty);

if c == 0
    detected = 0;
    disp('no chessboard detected');
    fenstrings = {};
    return;
end

fenstrings = {zeros(size(detectedchessboards, 1), 1)};
    
for i = 1:size(detectedchessboards, 1)
    disp('Performing classification');
    chesspieces = detectchesspieces(detectedchessboards{i});
    disp('Extracting fenstring');
    fenstrings{i} = createfenstring(chesspieces);
end

detected = 1;

end
