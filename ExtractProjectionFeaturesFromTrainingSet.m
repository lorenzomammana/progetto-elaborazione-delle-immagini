close all;
clear variables;
CreateLabels;

[images, labels] = readlists();

nimages = numel(images);

projection = zeros(nimages, 126);
hog = zeros(nimages, 1296);

for n = 1 : nimages
  im = imread(['trainingImagesCMYK\', images{n}]);
  im = imbinarize(im);
  pV = sum(im, 1);
  pH = sum(im, 2);
  projection(n, :) = [pV, pH'];
  hog(n, :) = extractHOGFeatures(im2double(im));
end

clear im n nimages;

save('data.mat');
