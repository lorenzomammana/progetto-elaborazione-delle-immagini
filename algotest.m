function [allout, cont] = algotest(j, k, pretty)

cont = 0;
allout = cell(k - j + 1, 1);
for i = j:k
   if i < 10
       str = sprintf('Foto/00%d.jpg', i);
   else
       str = sprintf('Foto/0%d.jpg', i);
   end
   
   im = im2double(imread(str));
   [detected, out] = chessboarddetector(im, pretty);
   
   if detected == 1
       for n = 1:size(out, 1)
           cont = cont + 1;
           allout{cont} = out{n};
       end
       
       
       %        figure;
       %        for p = 1:size(out, 1)
       %            subplot(1, size(out, 1), p), imshow(out{p});
       %        end
       %        title(['output of image ', num2str(i)]);
   end
end

allout = allout(~cellfun('isempty', allout));
end