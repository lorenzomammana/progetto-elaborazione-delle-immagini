function fenstring = createfenstring(predictedpieces)
tic
predictedpieces = reshape(predictedpieces, [8, 8]);
predictedpieces = predictedpieces';
f = load('fenconverter.mat');
fenconverter = f.fenconverter;
fenstring = '';

for i = 1:8
    emptyspace = 0;
    for j = 1:8
        if strcmp(cellstr(predictedpieces{i, j}), 'b') || ...
              strcmp(cellstr(predictedpieces{i, j}), 'w')
          emptyspace = emptyspace + 1;
        else
            if emptyspace > 0
                fenstring = strcat(fenstring, num2str(emptyspace));
            end
            emptyspace = 0;
            piece = cellstr(predictedpieces{i, j});
            fenstring = strcat(fenstring, fenconverter(piece{1}));
        end
    end
    
    if emptyspace > 0
        fenstring = strcat(fenstring, num2str(emptyspace));
    end
    
    if i < 8
        fenstring = strcat(fenstring, '/');
    end
end

fenstring = strcat(fenstring, ' - 0 1');
toc
end