function [images,labels] = readlists()
  f = fopen('images.list');
  z = textscan(f,'%s', 'Delimiter', ',');
  fclose(f);
  images = z{:}; 

  f = fopen('labels.list');
  l = textscan(f,'%s');
  labels = l{:};
  fclose(f);
end
