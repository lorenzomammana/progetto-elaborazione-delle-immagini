Files = dir('trainingImagesCMYK\');

FileNames = [];
for k = 3:length(Files)
label = strsplit(Files(k).name);
if ~strcmp(label{1}, 'b') && ~strcmp(label{1}, 'w')
    label{1} = label{1}(1:end-1);
end

FileNames = [FileNames, strcat(label{1}, '\n')];
end
fid = fopen('labels.list', 'wt');
fprintf(fid, FileNames);
fclose(fid);
fid = fopen('images.list', 'wt');
FileNames = [];
for k = 3:length(Files)
FileNames = [FileNames, strcat(Files(k).name, '\n')];
end
fprintf(fid, FileNames);
fclose(fid);