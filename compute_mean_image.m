function compute_mean_image()

close all;
clear variables;

[images, ~] = readlists();

sumImage = zeros(63, 63);
for n = 1 : nimages
  im = im2double(imread(['trainingImagesCMYK\', images{n}]));
  sumImage = sumImage + im;
end

meanimage = sumImage / nimages;

imwrite(meanimage, 'mean.jpg');

end
