function out = createtrainingimage(image, mean)
    image = image - mean;
    image = imresize(image, [227, 227], 'lanczos3');
    out = uint8(255 * image(:, :, [1 1 1]));
end