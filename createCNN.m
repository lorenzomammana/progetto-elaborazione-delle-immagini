function createCNN()

% Unused due to good theoretical performances but poor performances
% when testing with generic images

[images, labels] = readlists();

for i = 1:size(images, 1)
    images{i} = ['trainingImagesCMYK\', images{i}];
end

labels = categorical(labels);
imds = imageDatastore(images, 'Labels', labels);
mean = im2double(imread('mean.jpg'));

imds.ReadFcn = @(loc)createtrainingimage(im2double(imread(loc)), mean);


[trainingImages, validationImages] = splitEachLabel(imds, 0.7,...
                                                    'randomized');
                                               
net = alexnet;
layersTransfer = net.Layers(1:end-3);

numClasses = 14;
layers = [
    layersTransfer
    fullyConnectedLayer(numClasses, 'WeightLearnRateFactor', 20,...
                                    'BiasLearnRateFactor', 20)
    softmaxLayer
    classificationLayer];

miniBatchSize = 10;
numIterationsPerEpoch = floor(numel(trainingImages.Labels)/miniBatchSize);
options = trainingOptions('sgdm',...
    'MiniBatchSize',miniBatchSize,...
    'MaxEpochs',10,...
    'InitialLearnRate',1e-4,...
    'Verbose',false,...
    'Plots','training-progress',...
    'ValidationData',validationImages,...
    'ValidationFrequency',numIterationsPerEpoch);

CNN = trainNetwork(trainingImages, layers, options);

save('trainedCNN.mat', 'CNN');

end