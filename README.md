# Image processing project 2017/2018

### Requirement 
1. MATLAB
2. Optimization Toolbox
3. Image Processing Toolbox
4. Statistics and Machine Learning Toolbox
5. Computer Vision System Toolbox
6. Machine Vision Toolbox
7. Neural Network Toolbox(for CNN training, unused)

Only tested on Windows

### Test
finaltest(1, 61, false)

Perform detection and classification on the 61 images contained in Foto 
folder

Using true instead of false will create figures of algorithm steps,
this could require a lot of memory!

### Performance
Using KNN classifier trained with 1400 binary piece images 
validate with 5-fold-cross-validation.

Results details are in Analysis folder.
![confmat](https://gitlab.com/lorenzomammana/progetto-elaborazione-delle-immagini/raw/master/ConfusionMatrix.png)

