function cont = finaltest(j, k, pretty)

cont = 0;

for i = j:k
   if i < 10
       str = sprintf('Foto/00%d.jpg', i);
   else
       str = sprintf('Foto/0%d.jpg', i);
   end
   
   im = im2double(imread(str));
   
   [fenstrings, chessboards, detected] = extractfenfromimage(im, pretty);
   
   if detected
       if i < 10
           str = sprintf('Groundtruth/00%d.txt', i);
       else
           str = sprintf('Groundtruth/0%d.txt', i);
       end
       fid = fopen(str);
       realfen = fgetl(fid);

       disp(['Original FEN string of image ', num2str(i)]);
       disp(realfen);
       
       isbadrotated = true;
       for k = 1:size(fenstrings, 1)
          if EditDistance(realfen, fenstrings{k}) >= 18
              isbadrotated = isbadrotated && true;
          else
              isbadrotated = false;
          end
       end
       
       if isbadrotated
          disp('Correcting rotation');
          im = imrotate(chessboards{1}, 180);
          pieces = detectchesspieces(im);
          fenstrings{1} = createfenstring(pieces);
       end
       
       disp('Extracted FEN string');
       for k = 1:size(fenstrings, 2)
           disp(fenstrings{k});
           disp(['Edit distance result : ', ...
               num2str(EditDistance(realfen, fenstrings{k}))]);
       end
   end
end