function out = detectchesspieces(image)
warning('off', 'stats:pdist2:DataConversion')
tic
KNNclass = load('KNNclassificator.mat');
% CNNclass = load('trainedCNN.mat');
image = imresize(image, [518, 518]);
cmyk = rgb2cmyk(image);
cmyk = cmyk(:, :, 4);
[p, x, ~] = detectSquareCorners2(imbinarize(cmyk));
[H, ~] = homography(p, x);
image = homwarp(H, image);
% mean = im2double(imread('mean.jpg'));
image = imcrop(image, [9, 9, 501, 501]);

pieces = {zeros(1, 64)};
for i = 1:64
   posx = mod((i - 1), 8)  * 63;
   posy = floor((i - 1) / 8) * 63;
   piece = imcrop(image, [posx, posy, 63, 63]);
   piece = imresize(piece, [63, 63]);
%  t = piece;
   piece = rgb2cmyk(piece);
   piece = piece(:, :, 4);
   piece = imbinarize(piece);
   pV = sum(piece, 1);
   pH = sum(piece, 2);
   projection = [pV, pH'];
   hog = extractHOGFeatures(im2double(piece));
   pieces{i} = predict(KNNclass.KNN, [projection, hog]);
%    piece = createtrainingimage(piece, mean);
%    pieces{i} = classify(CNNclass.CNN, piece);
% 
%  figure, subplot(1,2, 1), imshow(t), subplot(1, 2, 2), imshow(piece);
%  title(['classificato come ', cellstr(pieces{i})]);
end

toc
out = pieces;

end