function [detected, out] = chessboarddetector(image, pretty)
warning('off', 'MATLAB:nearlySingularMatrix');
disp('Detecting chessboards');
tic
if size(image, 3) ~= 3
    detected = 0;
    out = {};
    disp('Image is not rgb');
    return;
end

resizeFactor = 0.5;
% Resize
image = imresize(image, resizeFactor, 'lanczos3');
cmyk = rgb2cmyk(image);
cmyk = cmyk(:, :, 4);
% CLAHE
cmyk = adapthisteq(cmyk);
% Adaptive threshold using local first-order statistic
T = adaptthresh(cmyk, 0.4);
bw = imbinarize(cmyk, T);

if pretty
    figure, subplot(1, 2, 1), imshow(image);
    title('Original image');
end

stats = regionprops(bw, 'BoundingBox', 'Image');
boundingBox = cat(1, stats.BoundingBox);
perimeter = 2 * (boundingBox(:, 3) + boundingBox(:, 4));
% Bounding box threshold
minSideDiff = 250;
minPerimeter = 1000;
t1 = minSideDiff * resizeFactor;
t2 = minPerimeter * resizeFactor;
condition1 = abs(boundingBox(:, 3) - boundingBox(:, 4)) <= t1;
condition2 = perimeter(:) > t2;
while ~any(condition1 & condition2)
    minSideDiff = minSideDiff + 50;
    minPerimeter = minPerimeter - 500;
    t1 = minSideDiff * resizeFactor;
    t2 = minPerimeter * resizeFactor;
    condition1 = abs(boundingBox(:, 3) - boundingBox(:, 4)) <=  t1;
    condition2 = perimeter(:) > t2;
end

bbIndexes = find(condition1 & condition2);
boundingBox = boundingBox(condition1 & condition2, :);

if pretty
    subplot(1, 2, 2), imshow(bw);
    title('Potential chessboards');
    for k = 1 : size(boundingBox, 1)
      thisBB = boundingBox(k, :);
      rectangle('Position', [thisBB(1),thisBB(2),thisBB(3),thisBB(4)], ...
      'EdgeColor','r','LineWidth',2 )
    end
end

out = cell(size(boundingBox, 1), 1);
tchessboard = rgb2gray(im2double(imread('schema1.png')));
tchessboardP90 = rgb2gray(im2double(imread('schema1+90.png')));
tchessboardM90 = rgb2gray(im2double(imread('schema1-90.png')));
for i = 1:size(boundingBox, 1)
    Image = stats(bbIndexes(i)).Image;
    % Corner and shape detection
    [p, x, isSquare] = detectSquareCorners2(Image);
    if isSquare
        [H, ~] = homography(p, x);
        % Rotation/Perspective correction
        warp = homwarp(H, imcrop(image, boundingBox(i, :)));
        twarp = rgb2gray(imresize(warp, size(tchessboard)));
        corr = corr2(twarp, tchessboard);
        corrP90 = corr2(twarp, tchessboardP90);
        corrM90 = corr2(twarp, tchessboardM90);
        % Correlation threshold
        if corr > 0.3 || corrP90 > 0.3 || corrM90 > 0.3
            if corrP90 > corr && corrP90 > corrM90
                warp = imrotate(warp, 90);
            else
                if corrM90 > corr && corrM90 > corrP90
                    warp = imrotate(warp, -90);
                end
            end
            out{i} = warp;
        else 
            if corr > 0.15 || corrP90 > 0.15 || corrM90 > 0.15
                if corrP90 > corr && corrP90 > corrM90
                    warp = imrotate(warp, 90);
                else
                    if corrM90 > corr && corrM90 > corrP90
                        warp = imrotate(warp, -90);
                    end
                end
                out{i} = warp;
            end
        end
    end
end

toc
out = out(~cellfun('isempty', out));
if size(out, 1) == 0 
    out = image;
    detected = 0;
    return
end

if pretty
    figure;
    for i = 1:size(out, 1)
        subplot(1, size(out, 1), i), imshow(out{i});
        title(['Detection ', num2str(i)]);
    end
end

detected = 1;
end



