% finds where the sums and differences of the coordinates in the shape
% are maximized and minimized.
% https://it.mathworks.com/matlabcentral/answers/
% 130851-how-to-find-rectangular-corners
function [p, x, isSquare] = detectSquareCorners2(bw)

[I,J] = find(bw == 1);
IJ = [I,J];

[~, idx] = min(IJ * [1 1; -1 -1; 1 -1; -1 1].');

corners = IJ(idx,:);

tleftEdge = [corners(1, 2), corners(1, 1)];
brightEdge = [corners(2, 2), corners(2, 1)];
trightEdge = [corners(3, 2), corners(3, 1)];
bleftEdge  = [corners(4, 2), corners(4, 1)];

% Caso limite di scacchiera ruotata di 45�
if norm(tleftEdge - trightEdge) < 10
    temp = [trightEdge(1), size(bw, 1)];
    trightEdge = brightEdge;
    brightEdge = temp;
end

side1 = norm(tleftEdge - trightEdge);
side2 = norm(tleftEdge - bleftEdge);

formFactor = (4 * pi * side1^2) / (16 * side2^2);
% figure, imshow(bw);
% hold on;
% plot(tleftEdge(1), tleftEdge(2), 'r*', 'LineWidth', 2, 'MarkerSize', 15);
% plot(bleftEdge(1), bleftEdge(2), 'r*', 'LineWidth', 2, 'MarkerSize', 15);
% plot(brightEdge(1), brightEdge(2), 'r*', 'LineWidth', 2, 'MarkerSize', 15);
% plot(trightEdge(1), trightEdge(2), 'r*', 'LineWidth', 2, 'MarkerSize', 15);
isSquare = formFactor > 0.2 && formFactor < 1.2;

tleft = [0, 0];
tright = [size(bw, 2), 0];
bleft = [0, size(bw, 1)];
bright = [size(bw, 2), size(bw, 1)];
p = [tleftEdge', trightEdge', bleftEdge', brightEdge'];
x = [tleft', tright', bleft', bright'];

end